import Vue from 'vue';
import Router from 'vue-router';
import Navigation from '../components/navigation/Navigation';
import Footer from '../components/footer/Footer';
import CardView from '../components/cardbacks/CardView'
import Home from '../components/cardbacks/Home';
import CardDetails from '../components/cardbacks/CardDetails';
import AddCard from '../components/cardbacks/AddCard';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      components: {
        navigation: Navigation,
        default: Home,       
      },
    },
    {
      path: '/cardback/:id',
      name: 'Cardback',
      components: {
        navigation: Navigation,
        default: CardDetails,
      }
    } ,
    {
      path: '/cardbackview/:id',
      name: 'ViewCardback',
      components: {
        navigation: Navigation,
        default: CardView,
      },
    },
    {
      path: '/addcardback',
      name: 'AddCardback',
      components: {
        navigation: Navigation,
        default: AddCard,
      },
    }


    // {
    //   path: '/users/:id',
    //   name: 'User',
    //   components: {
    //     navigation: Navigation,
    //     default: UserDetails,
    //     footer: Footer,
    //   },
    // },

  ]
});
