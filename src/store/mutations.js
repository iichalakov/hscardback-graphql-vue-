import mutations from './mutation-types';

export default {
  [mutations.SET_LOADER](state, isLoading) {
    state.isLoading = isLoading;
  },
  [mutations.SET_CARDBACK_LIST](state, cardbackList) {
    state.cardbackList = cardbackList;
  },
  [mutations.SET_SELECTED_CARDBACK](state, cardback){
    state.selectedCardback = cardback;
  },
  [mutations.ADD_CARDBACK](state, cardback){
    state.cardbackList.push(cardback);
  },
  [mutations.UPDATE_CARDBACK](state, {index, cardback}){
    state.cardbackList.splice(index, 1, cardback);
  },
  [mutations.DELETE_CARDBACK](state, index){
    state.cardbackList.splice(index, 1);
  },
};
