export default {
  getUserIndex: (usersList, _id) =>
    usersList.findIndex(user => user._id === _id),

  getCardbackIndex: (cardbackList, _id) => cardbackList.findIndex(cardback => cardback._id === _id),
};
