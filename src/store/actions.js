import mutations from './mutation-types';
import actions from './action-types';
import dataService from '../api/data-service';
import graphQlService from '../api/graphql-service';
import getters from './getters';

export default {

    [actions.GET_CARDBACK_LIST]({commit}, params= "_id name description imgAnimated") {
        commit(mutations.SET_LOADER, true);
        graphQlService.getCardbackList(
            params,
            (response) => {
                commit(mutations.SET_LOADER, false);
                commit(mutations.SET_CARDBACK_LIST, response.data.cardbacks);
            },
            (error) => {
                commit(mutations.SET_LOADER, false);
                console.log(error);
            }
        )
    },
    [actions.ADD_CARDBACK]({commit}, params){
        commit(mutations.SET_LOADER, true);
        graphQlService.addCardback(
            params,
            (response) => {
                commit(mutations.SET_LOADER, false);
                commit(mutations.ADD_CARDBACK, response.data.addCardback);
            },
            (error) => {
                commit(mutations.SET_LOADER, false);
                console.log(error);
            }
        )
    },
    [actions.DELETE_CARDBACK]({commit, state}, params){
        commit(mutations.SET_LOADER, true);
        graphQlService.deleteCardback(
            params,
            (response) => {
                commit(mutations.SET_LOADER, false);
                const _id = response.data.deleteCardback._id;
                const removeIndex = 
                getters.getCardbackIndex(state.cardbackList, _id);
                commit(mutations.DELETE_CARDBACK, removeIndex);
            },
            (error) => {
                commit(mutations.SET_LOADER, false);
                console.log(error);
            }
        )
    },
    [actions.SAVE_CARDBACK]({commit, state}, params){
        commit(mutations.SET_LOADER, true);
        graphQlService.saveCardback(
            params,
            (response) => {
                commit(mutations.SET_LOADER, false);
                
            },
            (error) => {
                commit(mutations.SET_LOADER, false);
                console.log(error);
            }
        )
    },

};
