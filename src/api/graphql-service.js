/**
 * Created by alexanderpetrov on 24.03.19.
 */

import gql from 'graphql-tag';
import graphqlClient from './graphql';

export default {

    async getCardbackList(params, success, failure) {
        try {
            const response = await graphqlClient.query({
                query: gql`
                    query {
                        cardbacks{
                            ${params}
                        }
                    }
                `
            });
            success(response);
        } catch (error){
            failure(error);    
        }
    },

    async addCardback(variables, success, failure){
        try {
            const response = await graphqlClient.mutate({
                mutation: gql`mutation($name: String!,
                     $description: String!, $imgAnimated: String!){
                        addCardback(name: $name, 
                        description: $description, imgAnimated: $imgAnimated){
                        _id
                        name
                        description
                        imgAnimated
                    }    
                }`,
                variables,
            });
            success(response);

        } catch (error){
            failure(error);
        }
    },

    async deleteCardback(_id, success, failure){
        try {
            const response = await graphqlClient.mutate({
                mutation: gql`
                mutation($_id: String!){
                    deleteCardback(_id: $_id){
                        _id
                    }
                }
                `,
                variables: {
                    _id
                }
            });
            success(response);
        } catch (error) {
            failure(error);
        }
    },
    
    async saveCardback(variables, success, failure) {
        try {
            const response = await graphqlClient.mutate({
                mutation: gql`
                mutation($_id: String!, $name: String!, $description: String!,
                     $imgAnimated: String!){
                    editCardback(_id: $_id, name: $name, description: $description, 
                        imgAnimated: $imgAnimated){
                        _id
                    }
                }
                `,
                variables
            });
            success(response);
        } catch (error){
            failure(error);    
        }
    },
}
